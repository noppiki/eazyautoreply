import json
import logging
import time

import selenium
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

release = True
account = open('setting.txt').read()
service = account.split('\n')
print(service)
driver = webdriver.Remote


def get_version(key):
    for i in service:
        if key in i:
            return i.split(',')[1]
    return ''


def get_apk_path(key):
    for i in service:
        if key in i:
            return i.split(',')[2]
    return ''


def login():
    global driver
    driver = webdriver.Remote(
        command_executor='http://127.0.0.1:4723/wd/hub',
        desired_capabilities={
            "platformName": "Android",
            "platformVersion": get_version('eazy'),
            "deviceName": "Nexus 5X",
            "app": get_apk_path('eazy'),
            "appPackage": "jp.ezsns.chat",
            "appActivity": ".ui.SplashActivity"
        })

    time.sleep(10)

    driver.find_element_by_id('jp.ezsns.chat:id/activity_signup_btn_login').click()
    time.sleep(3)
    driver.find_element_by_id('jp.ezsns.chat:id/email').send_keys(get_id('eazy'))
    driver.find_element_by_id('jp.ezsns.chat:id/password').send_keys(get_password('eazy'))
    driver.find_element_by_id('jp.ezsns.chat:id/button_login').click()
    time.sleep(5)
    driver.find_element_by_id('com.android.packageinstaller:id/permission_allow_button').click()
    time.sleep(2)
    driver.back()
    #    driver.find_element_by_id('com.android.packageinstaller:id/permission_deny_button').click()
    try:
        driver.find_element_by_id('jp.ezsns.chat:id/news_done').click()
    except:
        print("not found news_done")
    driver.find_element_by_id('android:id/button2').click()
    driver.find_element_by_id('android:id/button1').click()
    print("Successed Login")


def login_tsubaki():
    global driver
    driver = webdriver.Remote(
        command_executor='http://127.0.0.1:4723/wd/hub',
        desired_capabilities={
            "platformName": "Android",
            "platformVersion": get_version('tsubaki'),
            "deviceName": "Nexus 5X",
            "app": get_apk_path('tsubaki'),
            "appPackage": "com.tbk.app",
            "appActivity": ".ui.SplashActivity"
        })

    time.sleep(10)
    driver.find_element_by_id('com.tbk.app:id/activity_signup_btn_login').click()
    time.sleep(3)
    driver.find_element_by_id('com.tbk.app:id/email_fake').send_keys(get_id('tsubaki'))
    driver.find_element_by_id('com.tbk.app:id/password_fake').send_keys(get_password('tsubaki'))
    driver.find_element_by_id('com.tbk.app:id/button_login').click()
    time.sleep(5)
    driver.find_element_by_id('com.android.packageinstaller:id/permission_allow_button').click()
    time.sleep(2)
    driver.find_element_by_id('com.android.packageinstaller:id/permission_deny_button').click()
    driver.find_element_by_id('com.tbk.app:id/news_done').click()
    driver.find_element_by_id('com.tbk.app:id/close').click()
    driver.find_element_by_id('android:id/button2').click()
    driver.find_element_by_id('android:id/button1').click()


def login_ochaberi():
    global driver
    driver = webdriver.Remote(
        command_executor='http://127.0.0.1:4723/wd/hub',
        desired_capabilities={
            "platformName": "Android",
            "platformVersion": get_version(),
            "deviceName": "Nexus 5X",
            "app": get_apk_path(),
            "appPackage": "jp.ezsns.chat",
            "appActivity": ".ui.SplashActivity"
        })

    time.sleep(5)

    driver.find_element_by_id('jp.beri.test:id/activity_signup_btn_login').click()
    time.sleep(3)
    driver.find_element_by_id('jp.beri.test:id/email').send_keys(get_id())
    driver.find_element_by_id('jp.beri.test:id/password').send_keys(get_password())
    driver.find_element_by_id('jp.beri.test:id/button_login').click()
    time.sleep(5)
    driver.find_element_by_id('com.android.packageinstaller:id/permission_allow_button').click()
    driver.find_element_by_id('jp.beri.test:id/news_done').click()
    driver.find_element_by_id('jp.beri.test:id/close').click()
    driver.find_element_by_id('jp.beri.test:id/bt_cancel').click()
    driver.find_element_by_id('jp.beri.test:id/bt_ok').click()


def login_bisser():
    global driver
    driver = webdriver.Remote(
        command_executor='http://127.0.0.1:4723/wd/hub',
        desired_capabilities={
            "platformName": "Android",
            "platformVersion": get_version('bisser'),
            "deviceName": "Nexus 5X",
            "app": get_apk_path('bisser'),
            "appPackage": "net.bisser.app",
            "appActivity": "com.bisser.ui.SplashActivity"
        })

    time.sleep(10)
    driver.find_element_by_id('net.bisser.app:id/activity_signup_btn_login').click()
    time.sleep(3)
    driver.find_element_by_id('net.bisser.app:id/email_fake').send_keys(get_id('bisser'))
    driver.find_element_by_id('net.bisser.app:id/password_fake').send_keys(get_password('bisser'))
    driver.find_element_by_id('net.bisser.app:id/button_login').click()
    time.sleep(5)
    driver.find_element_by_id('android:id/button2').click()
    driver.find_element_by_id('android:id/button1').click()


def get_password(key):
    for i in service:
        if key in i:
            return i.split(',')[4]
    return ''


def get_id(key):
    for i in service:
        if key in i:
            return i.split(',')[3]
    return ''


def set_appeal_time():
    driver.find_element_by_id('jp.ezsns.chat:id/collection_button_meetpeople_call_setting').click()
    driver.find_element_by_id('jp.ezsns.chat:id/cbx_is_voice').click()
    driver.find_element_by_id('jp.ezsns.chat:id/spinner').click()
    driver.find_elements_by_class_name("android.widget.TextView")[7].click()
    driver.find_element_by_id('jp.ezsns.chat:id/cv_navigation_bar_btn_right').click()


def set_appeal_time_tsubaki():
    driver.find_element_by_id('com.tbk.app:id/collection_button_meetpeople_call_setting').click()
    driver.find_element_by_id('com.tbk.app:id/cbx_is_voice').click()
    driver.find_element_by_id('com.tbk.app:id/cv_navigation_bar_btn_right').click()


def set_appeal_time_ochaberi():
    driver.find_element_by_id('jp.beri.test:id/collection_button_meetpeople_call_setting').click()
    driver.find_element_by_id('jp.beri.test:id/cbx_is_voice').click()
    driver.find_element_by_id('jp.beri.test:id/cv_navigation_bar_btn_right').click()


def set_appeal_time_bisser():
    driver.find_element_by_id('net.bisser.app:id/cv_navigation_bar_img_right').click()
    driver.find_element_by_id('net.bisser.app:id/phone_setting_btn').click()
    driver.find_element_by_id('net.bisser.app:id/cbx_is_voice').click()
    driver.find_element_by_id('net.bisser.app:id/cv_navigation_bar_btn_right').click()
    driver.find_element_by_id('net.bisser.app:id/cv_navigation_bar_img_left').click()


def is_appeal_time():
    global release
    driver.find_element_by_id('jp.ezsns.chat:id/collection_button_meetpeople_call_setting').click()
    try:
        appeal_state = driver.find_element_by_id('jp.ezsns.chat:id/tv_time_available').text
    except selenium.common.exceptions.NoSuchElementException:
        driver.find_element_by_id('jp.ezsns.chat:id/cv_navigation_bar_btn_right').click()
        if release:
            set_appeal_time()
            print("set appeal time is complete")
        return False
    driver.find_element_by_id('jp.ezsns.chat:id/cv_navigation_bar_btn_right').click()
    print("already set appeal time")
    return True


def is_appeal_time_tsubaki():
    global release
    driver.find_element_by_id('jp.ezsns.chat:id/collection_button_meetpeople_call_setting').click()
    try:
        appeal_state = driver.find_element_by_id('jp.ezsns.chat:id/tv_time_available').text
    except selenium.common.exceptions.NoSuchElementException:
        driver.find_element_by_id('jp.ezsns.chat:id/cv_navigation_bar_btn_right').click()
        if release:
            set_appeal_time()
        return False
    driver.find_element_by_id('jp.ezsns.chat:id/cv_navigation_bar_btn_right').click()
    return True


def is_appeal_time_bisser():
    global release
    driver.find_element_by_id('net.bisser.app:id/cv_navigation_bar_img_right').click()
    driver.find_element_by_id('net.bisser.app:id/phone_setting_btn').click()
    is_appeal = driver.find_element_by_id('net.bisser.app:id/cbx_is_voice').get_attribute("checked")
    driver.find_element_by_id('net.bisser.app:id/cv_navigation_bar_img_left').click()
    driver.find_element_by_id('net.bisser.app:id/cv_navigation_bar_img_left').click()
    return is_appeal


def get_keyword():
    return open('keyword.txt', 'r').read()


def save_name(text, service):
    try:
        names = json.load(open('send.json', 'r'))
    except FileNotFoundError:
        names = {service: []}
    names[service].append(text)
    json.dump(names, open('send.json', 'w'))


def get_send_message():
    return open('message.txt', 'r').read()


def attach_movie(choose):
    driver.find_element_by_id('jp.ezsns.chat:id/fragment_chat_btn_add').click()
    driver.tap(1, 900, 1200, 1000)
    driver.save_screenshot('ss_eazy.png')
    thumbs = driver.find_elements_by_class_name('android.widget.ImageView')
    for c in choose:
        thumbs[(int(c) - 1) * 2].click()
    driver.find_element_by_id('jp.ezsns.chat:id/done').click()


def attach_movie_tsubaki(choose):
    driver.find_element_by_id('com.tbk.app:id/fragment_chat_btn_add').click()
    driver.tap(1, 900, 1200, 1000)
    driver.save_screenshot('ss_tsubaki.png')
    thumbs = driver.find_elements_by_class_name('android.widget.ImageView')
    for c in choose:
        thumbs[(int(c) - 1) * 2].click()
    driver.find_element_by_id('com.tbk.app:id/done').click()


def attach_movie_bisser(choose):
    driver.tap(1, 50, 1680, 1000)
    driver.tap(1, 880, 1230, 1000)
    driver.save_screenshot('ss_bisser.png')
    driver.find_element_by_id('android:id/button1').click()
    thumbs = driver.find_elements_by_id('android.widget.ImageView')
    for c in choose:
        thumbs[(int(c) - 1)].click()
    driver.find_element_by_id('net.bisser.app:id/done').click()


def send_message():
    driver.find_element_by_id('jp.ezsns.chat:id/fragment_chat_edt_content').send_keys(get_send_message())
    driver.find_element_by_id('jp.ezsns.chat:id/fragment_chat_img_send').click()


def send_message_tsubaki():
    driver.find_element_by_id('com.tbk.app:id/fragment_chat_edt_content').send_keys(get_send_message())
    driver.find_element_by_id('com.tbk.app:id/fragment_chat_img_send').click()


def send_message_bisser():
    driver.tap(1, 550, 1680, 1000)
    driver.switch_to.active_element().send_keys(get_send_message())
    driver.switch_to.active_element().send_keys(Keys.ENTER)


def is_not_send(name, service):
    try:
        names = open('send.txt', 'r').read().split('\n')
    except FileNotFoundError:
        names = {service: []}

    if name in names[service]:
        return False
    return True


def get_movie_number():
    numbers = open('movie.txt', 'r').read().split('\n')
    return numbers


def check_message():
    driver.find_element_by_id('jp.ezsns.chat:id/cv_navigation_bar_img_right').click()
    chat_names = driver.find_elements_by_id('jp.ezsns.chat:id/sliding_menu_right_item_txt_user_name')
    chat_messages = driver.find_elements_by_id('jp.ezsns.chat:id/sliding_menu_right_item_txt_chat_to_me_content')
    print("current message comes" + len(chat_messages))
    for i in range(len(chat_messages)):
        if get_keyword() in chat_messages[i].text:
            if is_not_send(chat_names[i]):
                save_name(chat_names[i].text, "eazy")
                print("send to " + chat_names[i].text)
                chat_messages[i].click()
                attach_movie(get_movie_number())
                send_message()
                driver.find_element_by_id('jp.ezsns.chat:id/cv_navigation_bar_img_left').click()
    driver.find_element_by_id('jp.ezsns.chat:id/cv_navigation_bar_img_right').click()


def check_message_tsubaki():
    driver.find_element_by_id('com.tbk.app:id/cv_navigation_bar_img_right').click()
    chat_names = driver.find_elements_by_id('com.tbk.app:id/sliding_menu_right_item_txt_user_name')
    chat_messages = driver.find_elements_by_id('com.tbk.app:id/sliding_menu_right_item_txt_chat_to_me_content')
    for i in range(len(chat_messages)):
        if get_keyword() in chat_messages[i].text:
            if is_not_send(chat_names[i]):
                save_name(chat_names[i].text, "tsubaki")
                chat_messages[i].click()
                attach_movie_tsubaki(get_movie_number())
                send_message_tsubaki()
                driver.find_element_by_id('com.tbk.app:id/cv_navigation_bar_img_left').click()


def check_message_ochaberi():
    driver.find_element_by_id('jp.beri.test:id/cv_navigation_bar_img_right').click()
    chat_names = driver.find_elements_by_id('jp.beri.test:id/sliding_menu_right_item_txt_user_name')
    chat_messages = driver.find_elements_by_id('jp.beri.test:id/sliding_menu_right_item_txt_chat_to_me_layout')
    for i in range(len(chat_messages)):
        if get_keyword() in chat_messages[i].text:
            if is_not_send(chat_names[i]):
                save_name(chat_names[i].text, "ochaberi")
                chat_messages[i].click()
                attach_movie(get_movie_number())
                send_message()
                driver.find_element_by_id('jp.ezsns.chat:id/cv_navigation_bar_img_left').click()
    driver.find_element_by_id('jp.ezsns.chat:id/cv_navigation_bar_img_right').click()


def check_message_bisser():
    try:
        driver.find_element_by_id('net.bisser.app:id/btv_chat_noti').click()
        chat_names = driver.find_elements_by_id('net.bisser.app:id/sliding_menu_right_item_txt_user_name')
        chat_messages = driver.find_elements_by_id('net.bisser.app:id/sliding_menu_right_item_txt_chat_to_me_content')
        for i in range(len(chat_messages)):
            if get_keyword() in chat_messages[i].text:
                if is_not_send(chat_names[i]):
                    save_name(chat_names[i].text, "bisser")
                    chat_messages[i].click()
                    attach_movie_bisser(get_movie_number())
                    send_message_bisser()
                    driver.find_element_by_id('net.bisser.app:id/cv_navigation_bar_img_left').click()
        driver.find_element_by_id('net.bisser.app:id/cv_navigation_bar_img_left').click()
    except:
        return


def start():
    login()
    is_appeal_time()
    check_message()
    driver.quit()
    time.sleep(10)


def start_tsubaki():
    login_tsubaki()
    check_message_tsubaki()
    driver.quit()
    time.sleep(10)


def start_bisser():
    login_bisser()
    check_message_bisser()
    driver.quit()
    time.sleep(10)


def main():
    try:
        start()
        start_tsubaki()
        start_bisser()
    except:
        import traceback
        logging.basicConfig(level=logging.DEBUG,
                            filename="system.log",
                            format="%(asctime)s %(levelname)-7s %(message)s")
        logging.exception(traceback.format_exc())
        driver.quit()
        time.sleep(60)
        main()


main()
